db.fruits.insertMany([
		{
			"name" : "Apple",
			"color" : "Red",
			"stock" : 20,
			"price" : 40,
			"supplier_id" : 1,
			"onSale" : true,
			"origin" : ["Philippines", "Us"]
 		},

 		{
			"name" : "Banana",
			"color" : "Yellow",
			"stock" : 15,
			"price" : 20,
			"supplier_id" : 2,
			"onSale" : true,
			"origin" : ["Philippines", "Ecuador"]
 		},

 		{
			"name" : "Kiwi",
			"color" : "Green",
			"stock" : 25,
			"price" : 50,
			"supplier_id" : 1,
			"onSale" : true,
			"origin" : ["Us" , "China"]
 		},

 		{
			"name" : "Mango",
			"color" : "Yellow",
			"stock" : 10,
			"price" : 120,
			"supplier_id" : 2,
			"onSale" : false,
			"origin" : ["Philippines", "India"]
 		}

	]); 


// Fruits on sale count operator
db.fruits.aggregate([
		{ $match: {onSale: true}},
		{ $group: {_id: "$onSale", fruitsOnSale : {$sum: 1}}},
		{ $project: {_id: 0}}
	]);
// Stock More than 20
db.fruits.aggregate([
	{$match: {stock:{$gte : 20}}},
		{$group: {_id : "$stock", enoughStock: {$sum: 1}}},
		{ $project: {_id: 0}}
	]);



// Average price

db.fruits.aggregate([
		{ $match: {onSale: true}},
		{ $group: {_id: "$supplier_id", avg_price : {$avg: "$price"}}},
		{ $sort: {total : 1}}

		]);

// Highest price
db.fruits.aggregate([
		{ $match: {onSale: true}},
		{ $group: {_id: "$supplier_id", max_price : {$max: "$price"}}}
		

		]);

// Lowest price
db.fruits.aggregate([
		{ $match: {onSale: true}},
		{ $group: {_id: "$supplier_id", min_price : {$min: "$price"}}}
		

		]);
